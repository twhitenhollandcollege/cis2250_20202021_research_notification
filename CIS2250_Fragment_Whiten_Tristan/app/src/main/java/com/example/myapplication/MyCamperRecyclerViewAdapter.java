package com.example.myapplication;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


public class MyCamperRecyclerViewAdapter extends RecyclerView.Adapter<MyCamperRecyclerViewAdapter.ViewHolder> {

    private final String[] camperNames;
    private final String[] camperTypes;

    public MyCamperRecyclerViewAdapter(String[] names, String[] types) {
        camperNames = names;
        camperTypes = types;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = camperNames[position];
        holder.mIdView.setText(camperTypes[position]);
        holder.mContentView.setText(camperNames[position]);
    }

    @Override
    public int getItemCount() {
        return camperNames.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public String mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.item_number);
            mContentView = (TextView) view.findViewById(R.id.content);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}