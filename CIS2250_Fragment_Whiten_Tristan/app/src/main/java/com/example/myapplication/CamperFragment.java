package com.example.myapplication;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.RemoteInput;
import android.content.Context;
import android.icu.text.CaseMap;
import android.os.Build;
import android.os.Bundle;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.myapplication.dummy.DummyContent;

import java.util.Arrays;

/**
 * A fragment representing a list of Items.
 */
public class CamperFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CamperFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static CamperFragment newInstance(int columnCount) {
        CamperFragment fragment = new CamperFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }




    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);


        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            recyclerView.setAdapter(new MyCamperRecyclerViewAdapter(getResources().getStringArray(R.array.camper_names), getResources().getStringArray(R.array.camper_types)));
        }

        //Declare constants/variables
        String CHANNEL_ID = "channelID";
        String title = "Camper Application";
        Context context = view.getContext();
        NotificationChannel channel;
        NotificationManager manager;
        String camperNames[] = getResources().getStringArray(R.array.camper_names);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //Prepare constants for the channel's name, description, and importance
            CharSequence CHANNEL_NAME = getString(R.string.channel_name);
            String CHANNEL_DESCRIPTION = getString(R.string.description);
            int CHANNEL_IMPORTANCE = NotificationManager.IMPORTANCE_HIGH;

            //Pass in the ID, name, and importance into the NotificationChannel constructor
            channel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, CHANNEL_IMPORTANCE);

            //Pass in the description
            channel.setDescription(CHANNEL_DESCRIPTION);

            //Create a notification manager to let the channel interact with the notification bar
            manager = context.getSystemService(NotificationManager.class);

            //Connect the manager and channel.
            manager.createNotificationChannel(channel);

            String key = "key";
            CharSequence label = "label";

            RemoteInput input = new RemoteInput.Builder(key).setLabel(label).build();




            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                    .setSmallIcon(R.mipmap.ic_launcher_round)
                    .setContentTitle(title)
                    .setContentText("Loaded " + camperNames.length + " campers.")
                    .setPriority(NotificationCompat.PRIORITY_HIGH);

            NotificationManagerCompat noteManager = NotificationManagerCompat.from(context);

            noteManager.notify(1, builder.build());


        }
        else {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(view.getContext(), CHANNEL_ID)
                    .setSmallIcon(R.mipmap.ic_launcher).setPriority(NotificationCompat.PRIORITY_DEFAULT);

            NotificationManagerCompat noteManager = NotificationManagerCompat.from(view.getContext());

            noteManager.notify(1, builder.build());
        }




        return view;


    }


}